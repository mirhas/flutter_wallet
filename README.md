# flutter_wallet

<img src="images/adaptive_icon.png" height="120">

A Flutter project for Irhas's assessment.

## Supported platforms

- Android ([.apk](https://www.dropbox.com/s/yj3w094afco63u1/2023-03-24%20-%20Flutter%20Wallet.apk?dl=0))

## Installation

```bash
# Get dependencies
$ flutter pub get

# Run the Flutter app
$ flutter run
```

## Run scripts

```bash
$ flutter_scripts run
```

Read the documentation about [flutter_scripts](https://pub.dev/packages/flutter_scripts) for activation guide.

## VSCode configuration

Add a configuration to .vscode/settings.json to exclude generated files.

```json
{
  "files.exclude": {
    "**/*.freezed.dart": true,
    "**/*.g.dart": true
  }
}
```

## Getting Started with Flutter

A few resources to get started with Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
