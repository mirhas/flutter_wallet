import 'package:flutter/material.dart';

import 'page/pages.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/':
      return MaterialPageRoute(builder: (_) => const AuthPage());
    case '/login':
      return MaterialPageRoute(builder: (_) => const LoginPage());
    case '/language-settings':
      final args = settings.arguments as LanguageSettingsArgs;
      return MaterialPageRoute(
        builder: (_) => LanguageSettingsPage(locale: args.locale),
      );
    case '/deck':
      final args = settings.arguments as DeckArgs;
      return MaterialPageRoute(
        builder: (_) => DeckPage(index: args.index),
      );
    default:
      return MaterialPageRoute(
        builder: (_) => Scaffold(
          body: Center(
            child: Text('No route defined for ${settings.name}'),
          ),
        ),
      );
  }
}
