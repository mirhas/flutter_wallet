import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart' show kBaseUrl;
import '../repository/auth_repo.dart';

class AuthServices implements AuthRepo {
  @override
  Future<void> logOut() async {
    final prefs = await SharedPreferences.getInstance();

    late String sessionId;

    sessionId = prefs.getString('sessionId') ?? '';

    final url = Uri.parse('$kBaseUrl/rest/auth/logout');

    final headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      '_sessionId': sessionId,
    };

    final res = await http.delete(
      url,
      headers: headers,
    );

    if (res.statusCode != 204) {
      throw Exception('Failed to logout');
    }

    await prefs.clear();
  }

  @override
  Future<void> logIn({
    required String username,
    required String password,
    required Function(String) onError,
    required Function(String) onSuccess,
  }) async {
    final prefs = await SharedPreferences.getInstance();

    final url = Uri.parse('$kBaseUrl/rest/auth/login');

    final body = json.encode({
      'username': username,
      'password': password,
    });

    final headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };

    final res = await http.post(
      url,
      headers: headers,
      body: body,
    );

    if (res.statusCode != 200) {
      onError('Failed to login');
      throw Exception('Failed to login');
    }

    final data = json.decode(res.body);

    if (kDebugMode) {
      print('sessionId: ${data['id']}');
      print('accountId: ${data['accountId']}');
    }

    await prefs.setString('sessionId', data['id']);
    await prefs.setString('accountId', data['accountId']);

    onSuccess('Login success');
  }

  @override
  Future<bool> isLoggedIn() async {
    final prefs = await SharedPreferences.getInstance();

    if (prefs.getString('sessionId') == null) {
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  }
}
