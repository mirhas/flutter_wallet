import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../common.dart';
import '../provider/locale_provider.dart';

enum LocaleSetting { en, id }

class LanguageSettingsPage extends ConsumerStatefulWidget {
  static const routeName = '/language-settings';

  const LanguageSettingsPage({super.key, required this.locale});

  final String locale;

  @override
  LanguageSettingsPageState createState() => LanguageSettingsPageState();
}

class LanguageSettingsPageState extends ConsumerState<LanguageSettingsPage> {
  late LocaleSetting _locale;

  @override
  void initState() {
    super.initState();
    _locale = _getLocale(widget.locale);
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final localizations = AppLocalizations.of(context)!;
    final colorScheme = Theme.of(context).colorScheme;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          localizations.languageSettings,
          style: textTheme.titleLarge!.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          var languageCode =
              AppLocalizations.supportedLocales[index].languageCode;

          return RadioListTile<LocaleSetting>(
            value: _getLocale(languageCode),
            groupValue: _locale,
            tileColor: colorScheme.surface,
            title: Text(_getTitle(languageCode)),
            onChanged: (lang) {
              setState(() => _locale = lang!);
              _setLocale(languageCode);
            },
          );
        },
        itemCount: AppLocalizations.supportedLocales.length,
      ),
    );
  }

  LocaleSetting _getLocale(String languageCode) {
    switch (languageCode) {
      case 'id':
        return LocaleSetting.id;
      default:
        return LocaleSetting.en;
    }
  }

  String _getTitle(String locale) {
    final appLocalizations = AppLocalizations.of(context)!;

    switch (locale) {
      case 'id':
        return appLocalizations.indonesian;
      default:
        return appLocalizations.english;
    }
  }

  void _setLocale(String locale) {
    switch (locale) {
      case 'id':
        ref.read(localeProvider).setLocale(const Locale('id', 'ID'));
        return;
      default:
        ref.read(localeProvider).setLocale(const Locale('en', 'US'));
        return;
    }
  }
}

class LanguageSettingsArgs {
  LanguageSettingsArgs(this.locale);

  final String locale;
}
