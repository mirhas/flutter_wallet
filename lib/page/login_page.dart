import 'package:flutter_wallet/services/auth_services.dart';

import '../common.dart';
import '../shared/shared.dart' show TinyCircularProgressIndicator;
import 'deck_page.dart' show DeckArgs;

class LoginPage extends StatefulWidget {
  static const routeName = '/login';

  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _loggingIn = false;

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final localizations = AppLocalizations.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Login',
          style: textTheme.titleLarge!.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          TextButton.icon(
            onPressed: () {
              // Paste the mock credentials for development
              _usernameController.text = 'USER050999';
              _passwordController.text = 'krl4gbx';
              setState(() {});
            },
            icon: const Icon(Icons.paste),
            label: Text(localizations.paste),
          ),
          const SizedBox(width: 16)
        ],
      ),
      body: ListView(
        padding: const EdgeInsets.all(16),
        children: [
          Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                TextFormField(
                  controller: _usernameController,
                  keyboardType: TextInputType.name,
                  decoration: const InputDecoration(
                    hintText: 'JOHNDOE',
                    labelText: 'Username',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your username';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: _passwordController,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: const InputDecoration(
                    labelText: 'Password',
                    hintText: 'Password',
                  ),
                  obscureText: true,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your password';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 16),
                ElevatedButton(
                  onPressed: _usernameController.text.isNotEmpty &&
                          _passwordController.text.isNotEmpty
                      ? () => _onPressed()
                      : null,
                  child: _loggingIn
                      ? const TinyCircularProgressIndicator()
                      : const Text('Login'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _onPressed() {
    final auth = AuthServices();
    final scaffoldMessenger = ScaffoldMessenger.of(context);

    if (_loggingIn) return;

    setState(() => _loggingIn = true);

    if (_formKey.currentState!.validate()) {
      auth.logIn(
        username: _usernameController.text,
        password: _passwordController.text,
        onError: (text) {
          setState(() => _loggingIn = false);

          return scaffoldMessenger.showSnackBar(
            SnackBar(
              content: Text(text),
            ),
          );
        },
        onSuccess: (_) => Navigator.of(context).pushReplacementNamed(
          '/deck',
          arguments: DeckArgs(index: 0),
        ),
      );

      return;
    }

    scaffoldMessenger.showSnackBar(
      const SnackBar(
        content: Text('Form is not valid'),
      ),
    );

    setState(() => _loggingIn = false);
  }
}
