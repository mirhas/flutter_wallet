import '../common.dart';
import '../services/auth_services.dart';
import '../shared/shared.dart';
import 'language_settings_page.dart' show LanguageSettingsArgs;

class AccountPage extends StatefulWidget {
  const AccountPage({super.key});

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  bool _loggingOut = false;

  @override
  Widget build(BuildContext context) {
    final auth = AuthServices();
    final colorScheme = Theme.of(context).colorScheme;
    final localizations = AppLocalizations.of(context)!;
    final languageCode = '${Localizations.localeOf(context)}';
    final textTheme = Theme.of(context).textTheme;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          localizations.account,
          style: textTheme.titleLarge!.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              if (_loggingOut) return;

              setState(() => _loggingOut = true);

              auth.logOut().then(
                    (_) => Navigator.of(context)
                        .pushNamedAndRemoveUntil('/', (route) => false),
                  );
            },
            icon: _loggingOut
                ? const TinyCircularProgressIndicator()
                : Icon(
                    Icons.logout,
                    color: colorScheme.error,
                  ),
          ),
        ],
      ),
      body: ListView(
        children: [
          ListTile(
            onTap: () => Navigator.of(context).pushNamed('/language-settings',
                arguments: LanguageSettingsArgs(languageCode)),
            leading: const Icon(Icons.language),
            title: Text(localizations.languageSettings),
            trailing: const Icon(Icons.chevron_right),
          ),
        ],
      ),
    );
  }
}
