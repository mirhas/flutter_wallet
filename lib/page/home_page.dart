import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

import '../common.dart';
import '../model/wallet_user.dart';
import '../provider/user_provider.dart';

class HomePage extends ConsumerStatefulWidget {
  const HomePage({super.key});

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends ConsumerState<HomePage> {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final localizations = AppLocalizations.of(context)!;
    final AsyncValue<WalletUser> user = ref.watch(userProvider);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          localizations.home,
          style: textTheme.titleLarge!.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          ref.invalidate(userProvider);
        },
        child: user.when(
          data: (data) {
            var balance = NumberFormat.simpleCurrency(
              locale: 'id',
              decimalDigits: 0,
            ).format(data.balance);

            return ListView(
              padding: const EdgeInsets.all(16),
              children: [
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          data.username!,
                          style: textTheme.bodySmall,
                        ),
                        const SizedBox(height: 8),
                        Text(localizations.balance),
                        Text(
                          balance,
                          style: textTheme.titleMedium,
                        ),
                        const SizedBox(height: 8),
                        Text('${localizations.name}: ${data.name!}'),
                      ],
                    ),
                  ),
                ),
              ],
            );
          },
          error: (error, _) => Stack(
            children: [
              Center(
                child: Text('$error'),
              ),
              ListView()
            ],
          ),
          loading: () => const Center(
            child: CircularProgressIndicator(),
          ),
          skipLoadingOnRefresh: false,
        ),
      ),
    );
  }
}
