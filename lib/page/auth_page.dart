import 'package:flutter/material.dart';

import '../services/auth_services.dart';
import 'deck_page.dart' show DeckArgs;

class AuthPage extends StatefulWidget {
  static const routeName = '/';

  const AuthPage({super.key});

  @override
  State<AuthPage> createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  final _auth = AuthServices();

  @override
  void initState() {
    super.initState();
    _auth.isLoggedIn().then((isLoggedIn) {
      if (isLoggedIn) {
        Navigator.of(context).pushReplacementNamed(
          '/deck',
          arguments: DeckArgs(index: 0),
        );
      } else {
        Navigator.of(context).pushReplacementNamed('/login');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
