import '../common.dart';
import 'deck_widgets.dart';

class DeckPage extends StatefulWidget {
  static const routeName = '/deck';

  const DeckPage({super.key, required this.index});

  final int index;

  @override
  State<DeckPage> createState() => _DeckPageState();
}

class _DeckPageState extends State<DeckPage> {
  late int _index;

  final List<Widget> _pages = const [
    HomePage(),
    TransferPage(),
    HistoryPage(),
    AccountPage(),
  ];

  @override
  void initState() {
    super.initState();
    _index = widget.index;
  }

  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;

    return Scaffold(
      body: _pages.elementAt(_index),
      bottomNavigationBar: NavigationBar(
        selectedIndex: _index,
        onDestinationSelected: (index) => setState(() => _index = index),
        destinations: [
          NavigationDestination(
            icon: const Icon(Icons.home_outlined),
            selectedIcon: const Icon(Icons.home),
            label: localizations.home,
          ),
          NavigationDestination(
            icon: const Icon(Icons.send_outlined),
            selectedIcon: const Icon(Icons.send),
            label: localizations.transfer,
          ),
          NavigationDestination(
            icon: const Icon(Icons.history_outlined),
            selectedIcon: const Icon(Icons.history),
            label: localizations.history,
          ),
          NavigationDestination(
            icon: const Icon(Icons.account_circle_outlined),
            selectedIcon: const Icon(Icons.account_circle),
            label: localizations.account,
          ),
        ],
      ),
    );
  }
}

class DeckArgs {
  DeckArgs({required this.index});

  final int index;
}
