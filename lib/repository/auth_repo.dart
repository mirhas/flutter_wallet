abstract class AuthRepo {
  Future<void> logIn({
    required String username,
    required String password,
    required Function(String) onError,
    required Function(String) onSuccess,
  });

  Future<void> logOut();

  Future<bool> isLoggedIn();
}
