import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;

import '../constants.dart' show kBaseUrl;
import '../model/wallet_user.dart';

final userProvider = FutureProvider<WalletUser>((ref) async {
  final prefs = await SharedPreferences.getInstance();

  final sessionId = prefs.getString('sessionId') ?? '';
  final accountId = prefs.getString('accountId') ?? '';

  final url = Uri.parse('$kBaseUrl/rest/account/detail?id=$accountId');

  final headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    '_sessionId': sessionId,
  };

  final res = await http.get(url, headers: headers);

  if (res.statusCode != 200) {
    throw Exception('Failed to get user');
  }

  final data = json.decode(res.body);

  return WalletUser.fromJson(data);
});
