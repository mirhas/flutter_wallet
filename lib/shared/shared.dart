import 'package:flutter/material.dart';

class TinyCircularProgressIndicator extends StatelessWidget {
  const TinyCircularProgressIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 16,
      width: 16,
      child: CircularProgressIndicator(
        strokeWidth: 2.0,
      ),
    );
  }
}
