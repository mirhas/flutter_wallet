import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'common.dart';
import 'route.dart';
import 'theme.dart';
import 'provider/locale_provider.dart';

class App extends ConsumerWidget {
  const App({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final locale = ref.watch(localeProvider).locale;

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      locale: locale,
      title: 'Wallet Demo',
      themeMode: ThemeMode.system,
      theme: theme,
      darkTheme: darkTheme,
      onGenerateRoute: generateRoute,
      initialRoute: '/',
    );
  }
}
