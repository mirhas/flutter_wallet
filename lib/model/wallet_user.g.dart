// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wallet_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_WalletUser _$$_WalletUserFromJson(Map<String, dynamic> json) =>
    _$_WalletUser(
      id: json['id'] as String?,
      name: json['name'] as String?,
      username: json['username'] as String?,
      balance: (json['balance'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$$_WalletUserToJson(_$_WalletUser instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'username': instance.username,
      'balance': instance.balance,
    };
