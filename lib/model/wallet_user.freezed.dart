// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'wallet_user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

WalletUser _$WalletUserFromJson(Map<String, dynamic> json) {
  return _WalletUser.fromJson(json);
}

/// @nodoc
mixin _$WalletUser {
  String? get id => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  String? get username => throw _privateConstructorUsedError;
  double? get balance => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $WalletUserCopyWith<WalletUser> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WalletUserCopyWith<$Res> {
  factory $WalletUserCopyWith(
          WalletUser value, $Res Function(WalletUser) then) =
      _$WalletUserCopyWithImpl<$Res, WalletUser>;
  @useResult
  $Res call({String? id, String? name, String? username, double? balance});
}

/// @nodoc
class _$WalletUserCopyWithImpl<$Res, $Val extends WalletUser>
    implements $WalletUserCopyWith<$Res> {
  _$WalletUserCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? username = freezed,
    Object? balance = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      username: freezed == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      balance: freezed == balance
          ? _value.balance
          : balance // ignore: cast_nullable_to_non_nullable
              as double?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_WalletUserCopyWith<$Res>
    implements $WalletUserCopyWith<$Res> {
  factory _$$_WalletUserCopyWith(
          _$_WalletUser value, $Res Function(_$_WalletUser) then) =
      __$$_WalletUserCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? id, String? name, String? username, double? balance});
}

/// @nodoc
class __$$_WalletUserCopyWithImpl<$Res>
    extends _$WalletUserCopyWithImpl<$Res, _$_WalletUser>
    implements _$$_WalletUserCopyWith<$Res> {
  __$$_WalletUserCopyWithImpl(
      _$_WalletUser _value, $Res Function(_$_WalletUser) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? username = freezed,
    Object? balance = freezed,
  }) {
    return _then(_$_WalletUser(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      username: freezed == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      balance: freezed == balance
          ? _value.balance
          : balance // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_WalletUser implements _WalletUser {
  const _$_WalletUser({this.id, this.name, this.username, this.balance});

  factory _$_WalletUser.fromJson(Map<String, dynamic> json) =>
      _$$_WalletUserFromJson(json);

  @override
  final String? id;
  @override
  final String? name;
  @override
  final String? username;
  @override
  final double? balance;

  @override
  String toString() {
    return 'WalletUser(id: $id, name: $name, username: $username, balance: $balance)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_WalletUser &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.balance, balance) || other.balance == balance));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, name, username, balance);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_WalletUserCopyWith<_$_WalletUser> get copyWith =>
      __$$_WalletUserCopyWithImpl<_$_WalletUser>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_WalletUserToJson(
      this,
    );
  }
}

abstract class _WalletUser implements WalletUser {
  const factory _WalletUser(
      {final String? id,
      final String? name,
      final String? username,
      final double? balance}) = _$_WalletUser;

  factory _WalletUser.fromJson(Map<String, dynamic> json) =
      _$_WalletUser.fromJson;

  @override
  String? get id;
  @override
  String? get name;
  @override
  String? get username;
  @override
  double? get balance;
  @override
  @JsonKey(ignore: true)
  _$$_WalletUserCopyWith<_$_WalletUser> get copyWith =>
      throw _privateConstructorUsedError;
}
