import 'package:freezed_annotation/freezed_annotation.dart';

part 'wallet_user.freezed.dart';

part 'wallet_user.g.dart';

@freezed
class WalletUser with _$WalletUser {
  const factory WalletUser({
    String? id,
    String? name,
    String? username,
    double? balance,
  }) = _WalletUser;

  factory WalletUser.fromJson(Map<String, dynamic> json) =>
      _$WalletUserFromJson(json);
}
